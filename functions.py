import gzip
#En este archivo se encuentran varias funciones importantes para la ejecucion de los programas


# readfile: lee el archivo de entrada

def readfile():
    with gzip.open('branch-trace-gcc.trace.gz','rt') as f:
        lines = f.readlines()


    pc = []
    for x in lines:
        pc.append(x.split(' ')[0])
    pc = [int(i) for i in pc]

    branch_taken = []
    for x in lines:
        branch_taken.append(x.split(' ')[1])
    f.close()
    return [pc,branch_taken]

# Esta funcion recibe un array de T y N y las convierte en unos y ceros
def string_to_number(branch_taken):
    t_or_n = [0] * len(branch_taken)
    for i in range(len(branch_taken)):
        if 'T' in branch_taken[i]: t_or_n[i] = 1
    return t_or_n


# def maquina_estados_2bit(estado_actual, input):
#     estado_proximo = 0
#     if estado_actual == 0 and input == 0:
#         estado_proximo = 0
#     if estado_actual == 1 and input == 0:
#         estado_proximo = 0
#     if estado_actual == 2 and input == 0:
#         estado_proximo = 1
#     if estado_actual == 3 and input == 0:
#         estado_proximo = 2
#     if estado_actual == 0 and input == 1:
#         estado_proximo = 1
#     if estado_actual == 1 and input == 1:
#         estado_proximo = 2
#     if estado_actual == 2 and input == 1:
#         estado_proximo = 3
#     if estado_actual == 3 and input == 1:
#         estado_proximo = 3
#     return estado_proximo


# Esta se encarga de manejar la maquina de estdos de las tablas BHT
# estado_actual: estado en el que se encuentra la prediccion
# input: entrada a la maquina de estados (avanzar a taken o not taken)
# estado proximo: estado proximo que calcula la maquina de estados
def maquina_estados_2bit_t(estado_actual, input):
    estado_proximo = 'SN'
    if 'SN' in estado_actual and 'N' in input:
        estado_proximo = 'SN'
    if 'WN' in estado_actual and 'N' in input:
        estado_proximo = 'SN'
    if 'WT' in estado_actual and 'N' in input:
        estado_proximo = 'WN'
    if 'ST' in estado_actual and 'N' in input:
        estado_proximo = 'WT'
    if 'SN' in estado_actual and 'T' in input:
        estado_proximo = 'WN'
    if 'WN' in estado_actual and 'T' in input:
        estado_proximo = 'WT'
    if 'WT' in estado_actual and 'T' in input:
        estado_proximo = 'ST'
    if 'ST' in estado_actual and 'T' in input:
        estado_proximo = 'ST'
    return estado_proximo

# Esta se encarga de manejar la maquina de estdos del predictor por torneo
# estado_actual: estado en el que se encuentra la prediccion
# input: entrada a la maquina de estados (avanzar a Gshare o Pshare)
# estado proximo: estado proximo que calcula la maquina de estados
def maquina_estados_2bit_torneo(estado_actual, input):
    estado_proximo = 'SG'
    if 'SG' in estado_actual and 'G' in input:
        estado_proximo = 'SG'
    if 'WG' in estado_actual and 'G' in input:
        estado_proximo = 'SG'
    if 'WP' in estado_actual and 'G' in input:
        estado_proximo = 'WG'
    if 'SP' in estado_actual and 'G' in input:
        estado_proximo = 'WP'
    if 'SG' in estado_actual and 'P' in input:
        estado_proximo = 'WG'
    if 'WG' in estado_actual and 'P' in input:
        estado_proximo = 'WP'
    if 'WP' in estado_actual and 'P' in input:
        estado_proximo = 'SP'
    if 'SP' in estado_actual and 'P' in input:
        estado_proximo = 'SP'
    return estado_proximo



