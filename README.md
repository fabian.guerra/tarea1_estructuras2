git repository link: https://gitlab.com/fabian.guerra/tarea1_estructuras2

DESCRIPCION DEL PROGRAMA:
Este programa busca cuantificar la eficiencia de cuatro predictores de branches,por lo que se hace una simulacion con el comportamiento de los predictores bimodal, PSHARE, GSHARE y por torneo (entre GSHARE Y PSHARE). El diseno cuenta con la capacidad de ser parametrizable por medio de los argumentos de entrada, para evaluar distintos tamanos de tablas de historias y contadores.
El programa compara los resultados de las predicciones con los valores reales de un archivo que contiene la informacion de 16 millones de saltos, y obtiene metricas de rendimiento.
Con un simulador como el disenado, es posible observar que tipo de predictor funciona mejor para cada caso e incluso, se pueden determinar los tamanos de las tablas a utilizar que cumplan de mejor manera las predicciones




INSTRUCCIONES PARA SIMULAR:

con el archivo branch-trace-gcc.trace.gz en el mismo archivo, usar el comando:

	python main.py <parametros en cualquier orden>

donde los parametros se pasan como -parametro parametro

Los parametros son:
-s: numero de bits de direccion en la tabla bht
-ph: tamano de los registros de historia privada en la tabla pht
-gh: tamano del registro global de historia
-bp: tipo de predictor
-o: opcion de imprimir en archivo de texto o no


Ejemplo:

	python main.py -bp 1 -s 4 -gh 3 -ph 3 -o 1
