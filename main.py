import functions
import predictor
import sys

#[s, bp, gh, ph, o, n_lines, print_bool_int] = sys.argv
#if print_bool_int == 1: print_bool = True
#if print_bool_int == 0: print_bool = False
#Se leen los parametros de entrada de los argumentos
s = 0
bp = 0
gh = 0
ph = 0
o = 0
for i in range(len(sys.argv)): 
    if (sys.argv[i] == '-s'):s = int(sys.argv[i+1])
    if (sys.argv[i] == '-bp'): bp = int(sys.argv[i + 1])
    if (sys.argv[i] == '-gh'): gh = int(sys.argv[i + 1])
    if (sys.argv[i] == '-ph'): ph = int(sys.argv[i + 1])
    if (sys.argv[i] == '-o'): o = int(sys.argv[i + 1])
# s = 3
# bp = 0
# gh = 4
# ph = 3
# o = 1

if o == 1: print_bool = True
if o == 0: print_bool = False


# Para la impresion del tipo de prediccion:
if (bp == 0):
    pt = "BIMODAL"
if(bp == 1):
    pt = "PSHARE "
if(bp == 2):
    pt = "GSHARE "
if(bp == 3):
    pt = "TOURNEY"

# print("prueba maq estados")
# print(functions.maquina_estados_2bit_t('WN', 'T'))
# print("leyendo archivo")

#Lectura de archivos
[pc, branch_taken] = functions.readfile()
#convertir los T y N del archivo a unos y ceros
t_or_n = functions.string_to_number(branch_taken)
number_of_instructions = len(pc)


f = open("results.txt", "w+")
f.write("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\r\n")
f.write("------------ TAREA #1 -------------\r\n")
f.write("Nombre: Fabian Guerra Esquivel    |\r\n")
f.write("Carne: B53207                     |\r\n")
f.write("___________________________________\r\n")
f.write("\r\n")
f.write("___________________________________\r\n")
f.write("PREDICTION PARAMETERS             |\r\n")
f.write("-----------------------------------\r\n")
f.write("BHT size (entries): %d          \r\n" % (2**s))
f.write("Global history register size: %d \r\n" % gh)
f.write("Private history register size: %d\r\n" % ph)
f.write("Branch prediction type: %s \r\n" % pt)
f.write("-----------------------------------\r\n")
f.write("\r\n")
f.write("___________________________________\r\n")
f.write("SIMULATION RESULTS                |\r\n")
f.write("-----------------------------------\r\n")
f.close()

###################################################################
print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
print("------------ TAREA #1 -------------")
print("Nombre: Fabian Guerra Esquivel    |")
print("Carne: B53207                     |")
print("___________________________________")
print("")
print("___________________________________")
print("PREDICTION PARAMETERS             |")
print("-----------------------------------")
print("BHT size (entries):", (2**s), "         |")
print("Global history register size: ", gh, "|")
print("Private history register size: ", ph, "|")
print("Branch prediction type: ", pt, " |")
print("-----------------------------------")
print("")
print("___________________________________")
print("SIMULATION RESULTS                |")
print("-----------------------------------")

#Entrada a las funciones
if (bp == 0):
    predictor.bimodal(s,number_of_instructions,pc,branch_taken,print_bool)
if(bp == 1):
    predictor.pshare(s,ph,number_of_instructions,pc,t_or_n, branch_taken, print_bool)
if(bp == 2):
    predictor.gshare(s,gh,number_of_instructions,pc,t_or_n, branch_taken,print_bool)
if(bp == 3):
    predictor.tourney(s,ph,gh,number_of_instructions,pc,t_or_n, branch_taken,print_bool)


