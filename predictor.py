import functions


# predictor.py
# Definicion de las cuatro funciones para simular los predictores de salto
#




####################       PREDICTOR BIMODAL

# Predictor bimodal
# s: numero de bits de direcciones en la tabla bht
# number_of_instructions: cantidad de instrucciones
# pc: direccion de pc actual
# branch_taken: T o N del output
# print_bool: determina si se imprime en archivo de texto o no
def bimodal(s, number_of_instructions, pc, branch_taken, print_bool):
    f = open("results.txt", "a+")
    #calculo del tamano de la tabla bht
    table_size = 2 ** (s)
    # print("table size: ", table_size)
    # inicializando tabla con valores Strong not taken, de dos contadores
    bht_table = ['SN'] * table_size
    # print(bht_table)
    # history = [0 for x in range(number_of_instructions)]
    #inicializacion de las predicciones y contadores
    prediction = ''
    correct = ''
    hits_t = 0
    hits_n = 0
    miss_t = 0
    miss_n = 0
    b = 'T'
    #print("PC     | OUTPUT | PREDICTION | CORRECT  \r\n")
    if print_bool: f.write("PC        |   BHT   | OUTPUT | PREDICTION | CORRECT  \n")
    #Para cada instruccion:
    for i in range(number_of_instructions):
        #codigo no importante: sirve para impresiones:
        if 'T' in branch_taken[i]: b = 'T'
        if 'N' in branch_taken[i]: b = 'N'
        ############################
        # leo los ultimos n digitos de pc, con una mascara 2^s-1
        ultimos_bits_pc = int(pc[i]) & (2 ** s - 1)

        # Realiza una prediccion de acuerdo al valor en la tabla bht, en el indice obtenido de los ultimos bits de pc
        if 'SN' in bht_table[ultimos_bits_pc]: prediction = 'N'
        if 'ST'in bht_table[ultimos_bits_pc]: prediction = 'T'
        if 'WN' in bht_table[ultimos_bits_pc]: prediction = 'N'
        if 'WT' in bht_table[ultimos_bits_pc]: prediction = 'T'
        old = bht_table[ultimos_bits_pc]

        #ingresa la entrada de la tabla bht en la maquina de estados, para actualizar su valor
        bht_table[ultimos_bits_pc] = functions.maquina_estados_2bit_t(bht_table[ultimos_bits_pc], branch_taken[i])

        # Actualiza contadores
        if prediction in branch_taken[i]:
            correct = "correct"
            if 'T' in prediction:
                hits_t += 1
            if 'N' in prediction:
                hits_n += 1
        else:
            correct = "incorrect"
            if 'T' in prediction:
                miss_t += 1
            if 'N' in prediction:
                miss_n += 1
        # if i<n_lines:
        #print("{} | {} | {} | {}" .format(pc[i],b, prediction, correct))

        if print_bool and i<5000:
            f.write("{}|   {}    |   {}    |     {}      |  {}\n".format(pc[i], old, b, prediction, correct))

    #calculo de hits e impresion de los resultados
    total_hits = (hits_n + hits_t) * 1.00000



    print("-----------------------------------")
    print("Number of branches:", number_of_instructions)
    print("Correct prediction of taken branches: ", hits_t)
    print("Correct prediction of not taken branches: ", hits_n)
    print("Incorrect prediction of taken branches: ", miss_t)
    print("Incorrect prediction of taken branches: : ", miss_n)
    print("Total correct predicted branches: ", total_hits)
    print("Percentage of correct predictions: ", (total_hits/number_of_instructions))
    f.close()







    #####################################################################################
    #                           PSHARE
    #####################################################################################


# Predictor Pshare
# s: numero de bits de direcciones en la tabla bht
# ph: tamano de los registros en la tabla de historias privadas
# number_of_instructions: cantidad de instrucciones
# pc: direccion de pc actual
# t_or_n; 0 o 1 del output (branch_taken convertido a unos y ceros)
# branch_taken: T o N del output
# print_bool: determina si se imprime en archivo de texto o no
def pshare(s, ph, number_of_instructions, pc, t_or_n, branch_taken, print_bool):
    #print("PC     | OUTPUT | PREDICTION | CORRECT  \r\n")
    f = open("results.txt", "a+")
    # inicializacion de las tablas y contadores
    table_size = 2 ** s
    bht_table = ['SN'] * table_size
    prediction = ['']
    correct = ['']
    hits_t = 0
    hits_n = 0
    miss_t = 0
    miss_n = 0
    history = [0] * table_size
    b = 'T'
    if print_bool: f.write("PC        |   BHT   | OUTPUT | PREDICTION | CORRECT | dir  \n")
    for i in range(number_of_instructions):

        if 'T' in branch_taken[i]: b = 'T'
        if 'N' in branch_taken[i]: b = 'N'
        # print("/////////////////////////////////////////////////")
        # print("i: ", i)
        #calculo de los ultimos s bits de pc
        ultimos_bits_pc = int(pc[i]) & (2**s-1)
        #calculo de la direccion: ultimos bits xor historia // mascara and con ultimos s bits
        dir = (ultimos_bits_pc ^ history[ultimos_bits_pc]) & (2 ** s - 1)
        # print("ultimos bits de pc: " ,ultimos_bits_pc)

        # print("history [", ultimos_bits_pc, "]: ", history[ultimos_bits_pc])
        # print("dir: ", dir)
        # print("history[",ultimos_bits_pc,"]", history[ultimos_bits_pc])
        # actualizacion de la prediccion
        if ('WN') in bht_table[dir]: prediction = 'N'
        if ('WT') in bht_table[dir]: prediction = 'T'
        if ('SN') in bht_table[dir]: prediction = 'N'
        if ('ST') in bht_table[dir]: prediction = 'T'
        old = bht_table[dir]
        # print("estado actual: ", bht_table[dir])
        # print("entry: ", branch_taken[i])
        # print("result: ", functions.maquina_estados_2bit_t(bht_table[dir], branch_taken[i]))

        #actualizacion de los estados
        bht_table[dir] = functions.maquina_estados_2bit_t(bht_table[dir], branch_taken[i])
        # print("prediction: ")
        # print(prediction)
        # print("bht ")
        # print(bht_table)

        # actualizacion de contadores y impresion de resultados
        if prediction in branch_taken[i]:
            correct = "hit"
            if 'T' in prediction:
                hits_t += 1
            if 'N' in prediction:
                hits_n += 1
        else:
            correct = "miss"
            if 'T' in prediction:
                miss_t += 1
            if 'N' in prediction:
                miss_n += 1
        if print_bool and i<5000:
            f.write("{}|   {}    |   {}    |     {}      |  {}  | {} |\n".format(pc[i], old, b, prediction, correct, dir))
        # if i < n_lines:
        #print("{} | {} | {} | {}".format(pc[i], b, prediction, correct))
        # actualizacion de la historia
        if (i == 0):
            history[ultimos_bits_pc] = 0
        else:
            #desplazamiento a la izquierda
            history[ultimos_bits_pc] = history[ultimos_bits_pc] << 1
            #suma del bit de historia
            history[ultimos_bits_pc] = history[ultimos_bits_pc] + t_or_n[i]
            #mascara para que quede de ph bits
            history[ultimos_bits_pc] = history[ultimos_bits_pc] & (2 ** ph - 1)
    total_hits = (hits_n + hits_t) * 1.00000
    hit_rate = total_hits / number_of_instructions
    print("-----------------------------------")
    print("Number of branches:", number_of_instructions)
    print("Correct prediction of taken branches: ", hits_t)
    print("Correct prediction of not taken branches: ", hits_n)
    print("Incorrect prediction of taken branches: ", miss_t)
    print("Incorrect prediction of taken branches: : ", miss_n)
    print("Total correct predicted branches: ", total_hits)
    print("Percentage of correct predictions: ", hit_rate)
    f.close()










    #############################################################################
    #                       GSHARE
    ############################################################################

# Predictor Gshare
# s: numero de bits de direcciones en la tabla bht
# gh: tamano del registro de historia global en bits
# number_of_instructions: cantidad de instrucciones
# pc: direccion de pc actual
# t_or_n; 0 o 1 del output (branch_taken convertido a unos y ceros)
# branch_taken: T o N del output
# print_bool: determina si se imprime en archivo de texto o no
def gshare(s, gh, number_of_instructions, pc, t_or_n, branch_taken, print_bool):
    #print("PC     | OUTPUT | PREDICTION | CORRECT  \r\n")
    f = open("results.txt", "a+")
    table_size = 2 ** (s)
    bht_table = ['SN'] * table_size
    prediction = ['']
    correct = ''
    ght = 0
    hits_t = 0
    hits_n = 0
    miss_t = 0
    miss_n = 0
    b = 'T'
    if print_bool: f.write("PC        |   BHT   | OUTPUT | PREDICTION | CORRECT  \n")
    for i in range(number_of_instructions):     # ej: ght =                             1000101
        #print(bht_table)
        if i == 0:
            ght = 0
        else:
            ght = ght << 1              #desplazo a la izquierda                           10001010
            ght = ght + t_or_n[i-1]       #sumo 0 o 1 si hizo el salto o no                  10001011
            ght = ght & (2**gh - 1)      #and con unos, dependiendo de gh, ej: gh = 7    & 01111111
                                                                                #    = 00001011
        # print("ght: ", ght)
        if 'T' in branch_taken[i]: b = 'T'
        if 'N' in branch_taken[i]: b = 'N'
        dir = (ght ^ pc[i]) & (2**s -1)
        # print("dir: ", dir)
        if ('WN') in bht_table[dir]: prediction = 'N'
        if ('WT') in bht_table[dir]: prediction = 'T'
        if ('SN') in bht_table[dir]: prediction = 'N'
        if ('ST') in bht_table[dir]: prediction = 'T'
        # print("estado actual: ", bht_table[dir])
        # print("input: ", branch_taken[i])
        # print("result: ", functions.maquina_estados_2bit_t(bht_table[dir], branch_taken[i]))
        old = bht_table[dir]
        bht_table[dir] = functions.maquina_estados_2bit_t(bht_table[dir], branch_taken[i])
        # print("bht_table[", dir, "]", bht_table[dir])
        if prediction in branch_taken[i]:
            correct = "hit"
            if 'T' in prediction:
                hits_t += 1
            if 'N' in prediction:
                hits_n += 1
        else:
            correct = "miss"
            if 'T' in prediction:
                miss_t += 1
            if 'N' in prediction:
                miss_n += 1
        if print_bool and i<5000:
            f.write("{}|   {}    |   {}    |     {}      |  {}\n".format(pc[i],old, b, prediction, correct))
        # if i < n_lines:
        print("{} | {} | {} | {}".format(pc[i], b, prediction, correct))
    total_hits = (hits_n + hits_t) * 1.00000
    hit_rate = total_hits / number_of_instructions
    # print("prediction: ", prediction)
    print("-----------------------------------")
    print("Number of branches:", number_of_instructions)
    print("Correct prediction of taken branches: ", hits_t)
    print("Correct prediction of not taken branches: ", hits_n)
    print("Incorrect prediction of taken branches: ", miss_t)
    print("Incorrect prediction of taken branches: : ", miss_n)
    print("Total correct predicted branches: ", total_hits)
    print("Percentage of correct predictions: ", hit_rate)
    f.close()















    #############################################################################
    #                       TOURNEY
    ############################################################################


# Predictor Pshare
# s: numero de bits de direcciones en la tabla bht
# ph: tamano de los registros de las historias privadas en bits para el predictor pshare
# gh: tamano del registro de historia global en bits para el predictor gshare
# number_of_instructions: cantidad de instrucciones
# pc: direccion de pc actual
# t_or_n; 0 o 1 del output (branch_taken convertido a unos y ceros)
# branch_taken: T o N del output
# print_bool: determina si se imprime en archivo de texto o no
def tourney(s,ph, gh, number_of_instructions, pc, t_or_n, branch_taken, print_bool):
    #print("PC     | OUTPUT | PREDICTION | CORRECT  \r\n")
    f = open("results.txt", "a+")
    #inicializacion de las tablas,registros y contadores necesarios tanto para el gshare y pshare
    table_size = 2 ** (s)
    bht_table_gshare = ['SN' for y in range(table_size)]
    bht_table_pshare = ['SN' for y in range(table_size)]
    prediction_gshare = ''
    prediction_pshare = ''
    prediction_tourney = ['SP'] * table_size # inicia en strongly prefered PSHARE
    prediction = ''
    chosen_predictor = ''
    correct = ''
    history = [0] * table_size
    ght = 0
    hits_t = 0
    hits_n = 0
    total_hits = 0
    miss_t = 0
    miss_n = 0
    b = 'T'
    if print_bool: f.write("PC        | PREDICTOR |   GSHARE   |   PSHARE   | OUTPUT | PREDICTION | CORRECT  \n")
    # PSHARE: aqui se calculan las tablas de prediccion con la misma logica del pshare
    for i in range(number_of_instructions):
        if 'T' in branch_taken[i]: b = 'T'
        if 'N' in branch_taken[i]: b = 'N'
        # print("/////////////////////////////////////////////////")
        # print("i: ", i)

        ultimos_bits_pc = int(pc[i]) & (2**s-1)
        dir = (ultimos_bits_pc ^ history[ultimos_bits_pc]) & (2 ** s - 1)
        # print("ultimos bits de pc: " ,ultimos_bits_pc)

        # print("history [", ultimos_bits_pc, "]: ", history[ultimos_bits_pc])
        # print("dir: ", dir)
        # print("history[",ultimos_bits_pc,"]", history[ultimos_bits_pc])
        if ('WN') in bht_table_pshare[dir]: prediction_pshare = 'N'
        if ('WT') in bht_table_pshare[dir]: prediction_pshare = 'T'
        if ('SN') in bht_table_pshare[dir]: prediction_pshare = 'N'
        if ('ST') in bht_table_pshare[dir]: prediction_pshare = 'T'
        old_p = bht_table_pshare[dir]
        # print("estado actual: ", bht_table_pshare[dir])
        # print("entry: ", branch_taken[i])
        # print("result: ", functions.maquina_estados_2bit_t(bht_table_pshare[dir], branch_taken[i]))
        bht_table_pshare[dir] = functions.maquina_estados_2bit_t(bht_table_pshare[dir], branch_taken[i])
        if (i == 0):
            history[ultimos_bits_pc] = 0
        else:
            history[ultimos_bits_pc] = history[ultimos_bits_pc] << 1
            history[ultimos_bits_pc] = history[ultimos_bits_pc] + t_or_n[i]
            history[ultimos_bits_pc] = history[ultimos_bits_pc] & (2 ** ph - 1)

    #GSHARE: Aqui se calculan las tablas de prediccion con la misma logica del gshare anterior

        if i == 0:
            ght = 0
        else:
            ght = ght << 1              #desplazo a la izquierda                           10001010
            ght = ght + t_or_n[i-1]       #sumo 0 o 1 si hizo el salto o no                  10001011
            ght = ght & (2**gh - 1)      #and con unos, dependiendo de gh, ej: gh = 7    & 01111111
                                                                                #    = 00001011
        # print("ght: ", ght)
        if 'T' in branch_taken[i]: b = 'T'
        if 'N' in branch_taken[i]: b = 'N'
        dir = (ght ^ pc[i]) & (2**s -1)
        # print("dir: ", dir)
        if ('WN') in bht_table_gshare[dir]: prediction_gshare = 'N'
        if ('WT') in bht_table_gshare[dir]: prediction_gshare = 'T'
        if ('SN') in bht_table_gshare[dir]: prediction_gshare = 'N'
        if ('ST') in bht_table_gshare[dir]: prediction_gshare = 'T'
        # print("estado actual: ", bht_table[dir])
        # print("input: ", branch_taken[i])
        # print("result: ", functions.maquina_estados_2bit_t(bht_table[dir], branch_taken[i]))
        old_g = bht_table_gshare[dir]
        bht_table_gshare[dir] = functions.maquina_estados_2bit_t(bht_table_gshare[dir], branch_taken[i])


    #METAPREDICTOR
        # Los siguientes dos condicionales determinan cual es el predictor elegido de acuerdo con
        # el valor que se encuentra en la tabla del metapredictor
        if (('SP' in prediction_tourney[ultimos_bits_pc]) or ('WP' in prediction_tourney[ultimos_bits_pc])):
            prediction = prediction_pshare
            chosen_predictor = 'P'
        if (('SG' in prediction_tourney[ultimos_bits_pc]) or ('WG' in prediction_tourney[ultimos_bits_pc])):
            prediction = prediction_gshare
            chosen_predictor = 'G'
        # Los siguientes condicionaeles verifican las condiciones necesarias para cambiar los estados de la tabla
        # del metapredictor. Unicamente cambian cuando una esta correcta y la otra incorrecta o viceversa
        if prediction_pshare in branch_taken[i] and not ((prediction_gshare) in branch_taken[i]):prediction_tourney[ultimos_bits_pc] = functions.maquina_estados_2bit_torneo(prediction_tourney[ultimos_bits_pc], 'P')
        elif not (prediction_pshare in branch_taken[i]) and (prediction_gshare) in branch_taken[i]:prediction_tourney[ultimos_bits_pc] = functions.maquina_estados_2bit_torneo(prediction_tourney[ultimos_bits_pc], 'G')
        elif prediction_gshare in branch_taken[i] and not ((prediction_pshare) in branch_taken[i]):prediction_tourney[ultimos_bits_pc] = functions.maquina_estados_2bit_torneo(prediction_tourney[ultimos_bits_pc], 'G')
        elif not (prediction_gshare in branch_taken[i]) and (prediction_pshare) in branch_taken[i]:prediction_tourney[ultimos_bits_pc] = functions.maquina_estados_2bit_torneo(prediction_tourney[ultimos_bits_pc], 'P')

        # Se actualizan contadores de hits y misses y se hace la impresion de resultados
        if prediction in branch_taken[i]:
            correct = 'correct'
            if 'T' in prediction:
                hits_t += 1
            if 'N' in prediction:
                hits_n += 1
        else:
            correct = 'incorrect'
            if 'T' in prediction:
                miss_t += 1
            if 'N' in prediction:
                miss_n += 1
        if print_bool and i<5000:
            f.write("{}|     {}     |     {}     |     {}     |   {}    |     {}      |  {}\n".format(pc[i],chosen_predictor, old_g, old_p, b, prediction, correct))
        # if i < n_lines:
        # print("{} | {} | {} | {}".format(pc[i], b, prediction, correct))
    total_hits = (hits_n + hits_t) * 1.00000
    hit_rate = total_hits / number_of_instructions
    print("-----------------------------------")
    print("Number of branches:", number_of_instructions)
    print("Correct prediction of taken branches: ", hits_t)
    print("Correct prediction of not taken branches: ", hits_n)
    print("Incorrect prediction of taken branches: ", miss_t)
    print("Incorrect prediction of taken branches: : ", miss_n)
    print("Total correct predicted branches: ", total_hits)
    print("Percentage of correct predictions: ", hit_rate)
    f.close()




